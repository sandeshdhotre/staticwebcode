package com.amdocs;

import org.junit.Assert;
import org.junit.Test;
import com.amdocs.Calculator;

public class CalculatorTest {

    @Test
    public void testAdd() throws Exception {
        final Calculator cal = new Calculator();
        final int resp = cal.add();
        Assert.assertEquals(9, resp);
        Assert.assertEquals(9, resp);
    }

    @Test
    public void testSub() throws Exception {
        final Calculator cal = new Calculator();
        final int resp = cal.sub();
        Assert.assertEquals(3, resp);
    }
}

