package com.amdocs;

import org.junit.Assert;
import org.junit.Test;
import com.amdocs.Increment;

public class IncrementTest {

    @Test
    public void testDecreese() throws Exception {
        final Increment inc = new Increment();
        final int resp = inc.decreasecounter(0);
        Assert.assertEquals(1, resp);
    }

    @Test
    public void testDecreese2() throws Exception {
        final Increment inc = new Increment();
        final int resp = inc.decreasecounter(1);
        Assert.assertEquals(1, resp);
    }

    @Test
    public void testDecreese3() throws Exception {
        final Increment inc = new Increment();
        final int resp = inc.decreasecounter(2);
        Assert.assertEquals(1, resp);
    }
}

